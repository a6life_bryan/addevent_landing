var tintTextColorElements = ''
+'.description .zoom,'
+'.material-icons,'
+'.material-icons-outlined,'
+'.primary-color'
+'';
var tintBackgroundColorElements = ''
+'.addeventatc:active,'
+'.addeventatc:hover,'
+'.addeventatc,'
+'.headline,'
+'.primary-color-bg'
+'';
var color = document.location.hash;
var theme = document.createElement('style');
theme.textContent = tintBackgroundColorElements+'{background-color:'+color+'!important;}'+tintTextColorElements+'{color:'+color+'!important;}';
document.head.appendChild(theme);